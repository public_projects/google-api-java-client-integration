package org.green.google.api.config;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration {
    @Bean
    public GeoApiContext getGeoApiContext(@Value("${api.google.location.key}") String apiKey){
        GeoApiContext context = new GeoApiContext.Builder()
                                        .apiKey(apiKey)
                                        .build();
        return context;
    }

    @Bean
    public Gson getGson(){
        return new GsonBuilder().setPrettyPrinting().create();
    }
}
