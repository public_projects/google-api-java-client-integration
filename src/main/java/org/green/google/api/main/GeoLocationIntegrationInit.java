package org.green.google.api.main;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.maps.GeoApiContext;
import com.google.maps.GeocodingApi;
import com.google.maps.model.GeocodingResult;
import com.google.maps.model.LatLng;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;


@Slf4j
@Component
public class GeoLocationIntegrationInit implements CommandLineRunner {
    @Autowired
    private GeoApiContext geoApiContext;

    @Autowired
    private Gson gson;

    @Override
    public void run(String... args) throws Exception {
        LatLng latLng = new LatLng(43.522338, 16.434267);
        GeocodingResult[] results = GeocodingApi.reverseGeocode(geoApiContext, latLng).await();
        log.info("Getting the address: {}", gson.toJson(results[0].addressComponents));
    }

    private void defaultExample() throws Exception {
        GeocodingResult[] results = GeocodingApi.geocode(geoApiContext, "1600 Amphitheatre Parkway Mountain View, CA 94043").await();
        log.info("Getting the address: {}", gson.toJson(results[0].addressComponents));
    }
}
